<?php

// Index
$app->get('/', 'HomeController:getIndex')->setName('index');

// Pages
$app->get('/about', 'PageController:getAbout');
$app->get('/rehearsals', 'PageController:getRehearsals');

$app->get('/recording', 'PageController:getRecording');
$app->get('/recording/studio', 'PageController:getStudio');
$app->get('/recording/equipment', 'PageController:getEquipment');

// Contact
$app->get('/contact', 'PageController:getContact');
$app->post('/contact', 'PageController:postContact');