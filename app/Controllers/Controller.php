<?php

namespace Yard\Controllers;

/**
 * Class Controller
 * @package Yard\Controllers
 */
class Controller
{
    /**
     * @var
     */
    protected $container;

    /**
     * Controller Constructor
     * @param $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * @param $property
     * @return bool
     */
    public function __get($property)
    {
        if ($this->container->{$property}) {
            return $this->container->{$property};
        }

        return false;
    }
}