<?php

namespace Yard\Controllers;

use Yard\Controllers\Controller;

/**
 * Class PageController
 * @package Yard\Controllers
 */
class PageController extends Controller
{
    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    public function getAbout($request, $response)
    {
        // $this->logger->addInfo('The home page was rendered.');
        return $this->renderer->render($response, "/templates/nav/about.php");
    }

    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    public function getRehearsals($request, $response)
    {
        return $this->renderer->render($response, "/templates/nav/rehearsals.php");
    }

    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    public function getRecording($request, $response)
    {
        return $response->withRedirect('/recording/studio');
    }

    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    public function getStudio($request, $response)
    {
        return $this->renderer->render($response, "/templates/nav/recording/studio.php");
    }

    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    public function getEquipment($request, $response)
    {
        return $this->renderer->render($response, "/templates/nav/recording/equipment.php");
    }

    /**]
     * @param $request
     * @param $response
     * @return mixed
     */
    public function getContact($request, $response)
    {
        return $this->renderer->render($response, "/templates/nav/contact.php");
    }

    /**
     * @param $request
     * @param $response
     * @return bool
     */
    public function postContact($request, $response){
        return true;
    }
}