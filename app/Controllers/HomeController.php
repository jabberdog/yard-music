<?php

namespace Yard\Controllers;

use Yard\Controllers\Controller;

/**
 * Class HomeController
 * @package Yard\Controllers
 */
class HomeController extends Controller
{
    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    public function getIndex($request, $response)
    {
        // $this->logger->addInfo('The home page was rendered.');
        return $this->renderer->render($response, "/templates/index.php");
    }
}