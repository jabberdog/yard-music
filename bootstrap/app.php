<?php

require __DIR__ . '/../vendor/autoload.php';

/**
 * Start PHP Sessions (Server)
 */
session_start();

/**
 * Configurations
 */
$configuration = array(
    'settings' => array(
        'displayErrorDetails' => true,
    ),
);

/**
 * Dependency Container
 */
$container = new \Slim\Container($configuration);
$app = new \Slim\App($container);

/**
 * Views - Php Renderer
 * @param $container
 * @return \Slim\Views\PhpRenderer
 */
$container['renderer'] = function ($container) {
  return new \Slim\Views\PhpRenderer('../app/views');
};

/**
 * Logging
 * @param $container
 * @return \Monolog\Logger
 */
$container['logger'] = function($container)
{
    $logger = new \Monolog\Logger('logger');
    $file_handler = new \Monolog\Handler\StreamHandler('../logs/app.log');
    $logger->pushHandler($file_handler);

    return $logger;
};

/**
 * Controllers
 * @param $container
 * @return \Yard\Controllers\HomeController
 */
$container['HomeController'] = function ($container) {
    return new \Yard\Controllers\HomeController($container);
};

$container['PageController'] = function ($container) {
    return new \Yard\Controllers\PageController($container);
};

/**
 * Routes
 */
include __DIR__ . '/../app/routes.php';